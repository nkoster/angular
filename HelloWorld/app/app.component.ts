import {Component} from 'angular2/core';
import {City} from './model/city.model';

@Component({
	selector: 'hello-world',
	templateUrl: 'app/app.component.html'
})

export class AppComponent {

	toggleMsg: string = 'Hide list';
	showCities: boolean = true;
	title: string = 'My Cities';
	cities: City[];
	newCity: string = '';
	textVisible = true;
	currentCity: City;
	cityPhoto: string = '';

	constructor() {
		this.cities = [
			new City(1, 'Almere', 'Flevoland'),
			new City(2, 'Amsterdam', 'Noord-Holland'),
			new City(3, 'Maastricht', 'Limburg'),
			new City(4, 'Den Haag', 'Zuid-Holland')
		]
	}

	toggleCities() {
		this.showCities = !this.showCities;
		this.showCities
			? this.toggleMsg = 'Hide list'
			: this.toggleMsg = 'Show list';
	}

	showCity(city:City) {
		this.currentCity = city;
		this.cityPhoto = `img/${this.currentCity.name}.png`;
	}

	changeCity(value:string) {
		this.newCity = value;
	}

	addCity(value:string) {
		let newCity = new City(
			this.cities.length + 1,
			value,
			'Onbekend'
		);
		this.cities.push(newCity);
	}

	toggleText() {
		this.textVisible = !this.textVisible;
	}
}
